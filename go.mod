module gitlab.com/yvesf/json-schema-compare

require (
	github.com/pkg/errors v0.8.1
	github.com/santhosh-tekuri/jsonschema v1.2.4
	github.com/stretchr/testify v1.3.0
)
