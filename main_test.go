package json_schema_compare

import (
	"bytes"
	"fmt"
	"github.com/santhosh-tekuri/jsonschema"
	"github.com/stretchr/testify/require"
	"testing"

	"github.com/stretchr/testify/assert"
)

type C struct {
	c   *jsonschema.Compiler
	ctr int
}

func Make() C {
	return C{jsonschema.NewCompiler(), 0}
}

func (c *C) Compile(t *testing.T, body string) *jsonschema.Schema {
	c.ctr++
	url := fmt.Sprintf(`http://test/%d`, c.ctr)
	err := c.c.AddResource(url, bytes.NewBufferString(body))
	require.NoError(t, err)
	s, err := c.c.Compile(url)
	require.NoError(t, err)
	return s
}
func TestOneOf(t *testing.T) {
	c := Make()
	t.Run(`Test valid`, func(t *testing.T) {
		subset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 5 },
    { "type": "number", "multipleOf": 3 },
    { "type": "number", "multipleOf": 1 }
  ]}`)
		superset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 5 },
    { "type": "number", "multipleOf": 3 }
  ]}`)
		assert.Empty(t, Compare(subset, superset))
	})
	t.Run(`Test invalid 1`, func(t *testing.T) {
		subset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 3 },
    { "type": "number", "multipleOf": 10 }
  ]}`)
		superset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 5 },
    { "type": "number", "multipleOf": 3 }
  ]}`)
		require.Len(t, Compare(subset, superset), 1)
		assert.Contains(t, Compare(subset, superset)[0].message, "oneOf[0] in superset has no counterpart in subset")
	})
	t.Run(`Test valid 2`, func(t *testing.T) {
		subset := c.Compile(t, `{
	"title":"test",
"description":"asd",
  "oneOf": [
    { "type": "number", "multipleOf": 1, "title":"subset" }
  ]}`)
		superset := c.Compile(t, `{
	"title":"test",
  "oneOf": [
    { "type": "number", "multipleOf": 5, "title":"superset" }
  ]}`)
		assert.Empty(t, Compare(subset, superset))
	})
	t.Run(`Test invalid 2`, func(t *testing.T) {
		subset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 5 }
  ]}`)
		superset := c.Compile(t, `{
  "oneOf": [
    { "type": "number", "multipleOf": 1 }
  ]}`)
		require.Len(t, Compare(subset, superset), 1)
		assert.Contains(t, Compare(subset, superset)[0].message, "oneOf[0] in superset has no counterpart in subset")
	})
}

func TestMultipleOf(t *testing.T) {
	c := Make()
	t.Run(`Test valid`, func(t *testing.T) {
		subset := c.Compile(t, `{ "type": "number", "multipleOf": 1 }`)
		superset := c.Compile(t, `{ "type": "number", "multipleOf": 5 }`)
		assert.Empty(t, Compare(subset, superset))
	})
	t.Run(`Test invalid`, func(t *testing.T) {
		subset := c.Compile(t, `{ "type": "number", "multipleOf": 5 }`)
		superset := c.Compile(t, `{ "type": "number", "multipleOf": 1 }`)
		require.Len(t, Compare(subset, superset), 1)
		assert.Contains(t, Compare(subset, superset)[0].message, "5 is not in 1")
	})
}
