# json-schema-compare


Superset.json
```json
{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Schema 1 / Superset",
    "$id": "http://example.com/superset",
    "type": "object",
    "properties": {
        "country": {
            "type": "string",
            "title": "The Country"
        },
        "name": {
            "type": "string",
            "title": "The the name"
        }
    }
}
```

Subset.json
```json
{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Schema 1 / Subset",
    "$id": "http://example.com/subset",
    "properties": {
        "country": {
            "type": "string"
        }
    }
}
```


Exitcode 0: `go run ./... -subset Subset.json -superset Superset.json`

- If we change the name of `country` in `Superset.json` to `area`:

```
$ go run ./... -subset Subset.json -superset Superset.json
/country                                                    : field is present in subset but not in superset
$ echo $?
1
```
