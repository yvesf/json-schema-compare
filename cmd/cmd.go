package main

import (
	"flag"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/yvesf/json-schema-compare"
	"os"
	"strings"
)

type resources map[string]string

func (r *resources) String() string {
	var out []string
	for uri, localPath := range *r {
		out = append(out, fmt.Sprintf(`uri=%s localPath=%s`, uri, localPath))
	}
	return strings.Join(out, `,`)
}

func (r resources) Set(value string) error {
	parts := strings.Split(value, `=`)
	if len(parts) != 2 {
		return errors.New(``)
	}
	r[parts[0]] = parts[1]
	return nil
}

var (
	Config = struct {
		resources      resources // map { uri: local-File-Path }
		schemaSubset   string
		schemaSuperset string
	}{}
)

func main() {
	flag.Var(&Config.resources, `resource`, `0..n resource files with uri "http://uri=local-filename"`)
	flag.StringVar(&Config.schemaSubset, `subset`, ``, ``)
	flag.StringVar(&Config.schemaSuperset, `superset`, ``, ``)
	err := flag.CommandLine.Parse(os.Args[1:])
	if err != nil {
		panic(err)
	}

	valErr, err := json_schema_compare.CompareFiles(Config.schemaSubset, Config.schemaSuperset, Config.resources)
	if err != nil {
		panic(err)
	}
	for _, err := range valErr {
		println(err.Error())
	}
	if len(valErr) > 0 {
		os.Exit(1)
	}
}
