package json_schema_compare

import (
	"fmt"
	"github.com/santhosh-tekuri/jsonschema"
	"os"
	"sort"
	"strings"
)

func foundString(s []string, needle string) bool {
	i := sort.SearchStrings(s, needle)
	return i < len(s) && needle == s[i]
}

type validationError struct {
	message string
	path    []string
}

func (e *validationError) Error() string {
	path := strings.Join(e.path, `/`)
	return fmt.Sprintf(`%-60s: %s`, path, e.message)
}

type validationErrors []validationError

func (errs *validationErrors) appendError(format string, path []string, args ...interface{}) {
	message := fmt.Sprintf(format, args...)
	*errs = append(*errs, validationError{message, path})
}

func (errs *validationErrors) notImplemented(path []string, field string) {
	errs.appendError(`FIXME `+field+` not implemented`, append(path, field))
}

func (errs *validationErrors) append(newErrs validationErrors) {
	*errs = append(*errs, newErrs...)
}

func CompareFiles(subsetUrl string, supersetUrl string, resources map[string]string) (validationErrors, error) {
	compiler := jsonschema.NewCompiler()
	for localPath, uri := range resources {
		in, err := os.Open(localPath)
		if err != nil {
			return nil, err
		}
		err = compiler.AddResource(uri, in)
		if err != nil {
			return nil, err
		}
	}
	subset, err := compiler.Compile(subsetUrl)
	if err != nil {
		return nil, err
	}
	superset, err := compiler.Compile(supersetUrl)
	if err != nil {
		return nil, err
	}

	return Compare(subset, superset), nil
}

func Compare(subset *jsonschema.Schema, superset *jsonschema.Schema) validationErrors {
	return compare3(resolveRef(subset), resolveRef(superset), []string{``})
}

func compare3(subset *jsonschema.Schema, superset *jsonschema.Schema, path []string) (errs validationErrors) {
	subset = resolveRef(subset)
	superset = resolveRef(superset)

	// The types must be equal
	sort.Strings(subset.Types)
	sort.Strings(superset.Types)
	for _, subType := range subset.Types {
		if !foundString(superset.Types, subType) {
			errs.appendError(`subset type %q is not in superset types %+v`, path, subType, superset.Types)
		}
	}
	for _, supersetType := range superset.Types {
		if !foundString(subset.Types, supersetType) {
			if supersetType == `object` && len(subset.Types) == 0 {
				continue // no type in subset is equal to object
			}
			errs.appendError(`superset type %q is not in subset types %+v`, path, supersetType, subset.Types)
		}
	}

	if foundString(subset.Types, `string`) {
		errs.append(compareString(subset, superset, path))
	}
	if foundString(subset.Types, `number`) {
		errs.append(compareNumber(subset, superset, path))
	}
	if foundString(subset.Types, `array`) {
		errs.append(compareArray(subset, superset, path))
	}
	if foundString(subset.Types, `object`) || len(subset.Types) == 0 {
		errs.append(compareObject(subset, superset, path))
	}

	// allOf: all of superset must be included in subset
	if len(subset.AllOf) > 0 && len(superset.AllOf) > 0 {
		for supersetIndex, supersetSchema := range superset.AllOf {
			var match = false
			for _, subsetSchema := range subset.AllOf {
				deepErrs := deepCompare(subsetSchema, supersetSchema, append(path, `allOf`))
				if len(deepErrs) == 0 {
					match = true
					break
				}
			}
			if !match {
				errs.appendError(`allOf[%d] in superset has no counterpart in subset`,
					append(path, `allOf`), supersetIndex)
			}
		}
	} else if len(subset.AllOf) > 0 || len(superset.AllOf) > 0 {
		errs.appendError(`allOf must appear in both subset and superset`, append(path, `allOf`))
	}

	// anyOf: all of superset must be included in subset
	if len(subset.AnyOf) > 0 && len(superset.AnyOf) > 0 {
		for supersetIndex, supersetSchema := range superset.AnyOf {
			var match = false
			for _, subsetSchema := range subset.AnyOf {
				deepErrs := deepCompare(subsetSchema, supersetSchema, append(path, `anyOf`))
				if len(deepErrs) == 0 {
					match = true
					break
				}
			}
			if !match {
				errs.appendError(`anyOf[%d] in superset has no counterpart in subset`,
					append(path, `anyOf`), supersetIndex)
			}
		}
	} else if len(subset.AnyOf) > 0 || len(superset.AnyOf) > 0 {
		errs.appendError(`anyOf must appear in both subset and superset`, append(path, `anyOf`))
	}

	// oneOf: all of superset must be included in subset
	if len(subset.OneOf) > 0 && len(superset.OneOf) > 0 {
		for supersetIndex, supersetSchema := range superset.OneOf {
			var match = false
			for _, subsetSchema := range subset.OneOf {
				deepErrs := deepCompare(subsetSchema, supersetSchema, append(path, `oneOf`))
				if len(deepErrs) == 0 {
					match = true
					break
				}
			}
			if !match {
				errs.appendError(`oneOf[%d] in superset has no counterpart in subset`,
					append(path, `oneOf`), supersetIndex)
			}
		}
	} else if len(subset.OneOf) > 0 || len(superset.OneOf) > 0 {
		errs.appendError(`oneOf must appear in both subset and superset`, append(path, `oneOf`))
	}

	// if subset defines a const then superset must define the same const's
	if len(subset.Constant) > 0 {
		if len(superset.Constant) == 0 {
			errs.appendError(`constant value in subset but not in superset`, append(path, `const`))
		} else if len(subset.Constant) != len(superset.Constant) {
			errs.appendError(`different constant value in schemas`, append(path, `const`))
		} else {
			for i := 0; i < len(subset.Constant); i++ {
				if subset.Constant[i] != superset.Constant[i] {
					errs.appendError(`different constant value [%d] in schemas`, append(path, `const`), i)
				}
			}
		}
	}

	return
}

func compareObject(subset *jsonschema.Schema, superset *jsonschema.Schema, path []string) (errs validationErrors) {
	if subset.AdditionalProperties != nil || superset.AdditionalProperties != nil {
		errs.appendError(`FIXME additionalProperties not implemented`, append(path, `additionalProperties`))
	}

	// if there is a field in subset that is not required in superset
	sort.Strings(superset.Required)
	for _, requiredField := range subset.Required {
		if !foundString(superset.Required, requiredField) {
			errs.appendError(`field %q is required in subset but not in superset`,
				append(path, `required`, requiredField), requiredField)
		}
	}

	// minProperties
	if subset.MinProperties != -1 {
		if superset.MinProperties == -1 {
			errs.appendError(`subset has minProperties %d but superset has no minimum`,
				append(path, `minProperties`), subset.MinProperties)
		} else if superset.MinProperties < subset.MinProperties {
			errs.appendError(`subset has minProperties %d but superset has minProperties %d`,
				append(path, `minProperties`), subset.MinProperties, superset.MinProperties)
		} // else: superset is smaller the subset. That's OK but makes no sense
	}

	// maxProperties
	if subset.MaxProperties != -1 {
		if superset.MaxProperties == -1 {
			errs.appendError(`subset has maxProperties %d but superset has no maxProperties`,
				append(path, `maxProperties`), subset.MaxProperties)
		} else if superset.MaxProperties > subset.MaxProperties {
			errs.appendError(`subset has maxProperties %d but superset has maxProperties %d`,
				append(path, `maxProperties`), subset.MaxProperties, superset.MaxProperties)
		} // else: superset is smaller the subset. That's OK but makes no sense
	}

	if len(subset.Dependencies) > 0 || len(superset.Dependencies) > 0 {
		errs.notImplemented(path, `dependencies`)
	}
	if len(subset.PatternProperties) > 0 || len(superset.PatternProperties) > 0 {
		errs.notImplemented(path, `patternProperties`)
	}

	if subset.Not != nil || superset.Not != nil {
		errs.notImplemented(path, `not`)
	}

	if subset.If != nil || superset.If != nil {
		if subset.If == nil && superset.If != nil {
			errs.appendError(`"if" in superset but not in subset`, append(path, `if`))
		} else if subset.If != nil && superset.If == nil {
			errs.appendError(`"if" in subset but not in superset`, append(path, `if`))
		} else {
			deepErrs := deepCompare(subset.If, superset.If, append(path, `if`))
			if len(deepErrs) > 0 {
				errs.appendError(`"if" is not totally equal`, append(path, `if`))
				errs = append(errs, deepErrs...)
			}
		}
	}
	if subset.Then != nil || superset.Then != nil {
		if subset.Then == nil && superset.Then != nil {
			errs.appendError(`"then" in superset but not in subset`, append(path, `then`))
		} else if subset.Then != nil && superset.Then == nil {
			errs.appendError(`"then" in subset but not in superset`, append(path, `then`))
		} else {
			deepErrs := deepCompare(subset.Then, superset.Then, append(path, `then`))
			if len(deepErrs) > 0 {
				errs.appendError(`"then" is not totally equal`, append(path, `then`))
				errs = append(errs, deepErrs...)
			}
		}
	}
	if subset.Else != nil || superset.Else != nil {
		if subset.Else == nil && superset.Else != nil {
			errs.appendError(`"else" in superset but not in subset`, append(path, `else`))
		} else if subset.Else != nil && superset.Else == nil {
			errs.appendError(`"else" in subset but not in superset`, append(path, `else`))
		} else {
			deepErrs := deepCompare(subset.Else, superset.Else, append(path, `else`))
			if len(deepErrs) > 0 {
				errs.appendError(`"else" is not totally equal`, append(path, `else`))
				errs = append(errs, deepErrs...)
			}
		}
	}

	for k, v1 := range subset.Properties {
		newPath := append(path, k)
		v2, ok := superset.Properties[k]
		if ok {
			// descend into properties
			errs.append(compare3(v1, v2, newPath))
		} else {
			errs.appendError(`field is present in subset but not in superset`, newPath)
		}
	}
	return
}

func compareString(subset *jsonschema.Schema, superset *jsonschema.Schema, path []string) (errs validationErrors) {
	if subset.FormatName != superset.FormatName {
		errs.appendError(`subset format %q != superset format %q`,
			append(path, `format`), subset.FormatName, superset.FormatName)
	}
	// if subset defines an enum then superset must contain the equal set or less values
	if subset.Enum != nil {
		subsetValues := make([]string, len(subset.Enum))
		for _, v := range subset.Enum {
			subsetValues = append(subsetValues, v.(string))
		}
		sort.Strings(subsetValues)
		for _, supersetEnumValue := range superset.Enum {
			if !foundString(subsetValues, supersetEnumValue.(string)) {
				errs.appendError(`superset enum value %q is not contained in subset`,
					append(path, `enum`), supersetEnumValue.(string))
			}
		}
	}

	// if subset has a pattern then superset must have the same pattern
	if subset.Pattern != nil {
		if subset.Pattern.String() != superset.Pattern.String() {
			errs.appendError(`subset pattern %q != superset pattern %q`,
				append(path, `format`), subset.Pattern.String(), superset.Pattern.String())
		}
	}

	if subset.MaxLength != -1 {
		if superset.MaxLength == -1 {
			errs.appendError(`subset has maxLength %d but superset has no limit`,
				append(path, `maxLength`), subset.MaxLength)
		} else if superset.MaxLength > subset.MaxLength {

			errs.appendError(`subset has maxLength %d but superset has maxLength %d`,
				append(path, `maxLength`), subset.MaxLength, superset.MaxLength)
		} // else: superset is smaller the subset. That's OK but makes no sense
	}

	if subset.MinLength != -1 {
		if superset.MinLength == -1 {
			errs.appendError(`subset has minLength %d but superset has no minLength`,
				append(path, `maxLength`), subset.MinLength)
		} else if superset.MinLength < subset.MinLength {
			errs.appendError(`subset has minLength %d but superset has minLength %d`,
				append(path, `maxLength`), subset.MinLength, superset.MinLength)
		} // else: superset is smaller the subset. That's OK but makes no sense
	}

	return
}

func compareNumber(subset *jsonschema.Schema, superset *jsonschema.Schema, path []string) (errs validationErrors) {
	// maximum
	if subset.Maximum != nil {
		if superset.Maximum == nil {
			errs.appendError(`subset has maximum %s but superset has no limit`,
				append(path, `maximum`), subset.Maximum.String())
		} else if superset.Maximum.Cmp(subset.Maximum) == 1 {
			errs.appendError(`subset has maximum %s but superset has maximum %s`,
				append(path, `maximum`), subset.Maximum.String(), superset.Maximum.String())
		} // else: superset is smaller the subset. That's OK but makes no sense
	}

	// minimum
	if subset.Minimum != nil {
		if superset.Minimum == nil {
			errs.appendError(`subset has minimum %s but superset has no minimum`,
				append(path, `minimum`), subset.Minimum.String())
		} else if superset.Minimum.Cmp(subset.Minimum) == -1 {
			errs.appendError(`subset has minimum %d but superset has minimum %d`,
				append(path, `minimum`), subset.Minimum.String(), superset.Minimum.String())
		}
	}

	if subset.ExclusiveMinimum != nil || superset.ExclusiveMinimum != nil {
		errs.notImplemented(path, `exclusiveMinimum`)
	}
	if subset.ExclusiveMaximum != nil || superset.ExclusiveMaximum != nil {
		errs.notImplemented(path, `exclusiveMaximum`)
	}

	// superset % subset == 0
	if subset.MultipleOf != nil || superset.MultipleOf != nil {
		sub := *subset.MultipleOf
		sup := *superset.MultipleOf
		if !(sup.IsInt() && sub.IsInt()) {
			errs.notImplemented(path, `multipleOf for non integer`)
		}
		iSub, _ := sub.Int64()
		iSup, _ := sup.Int64()

		if iSup%iSub != 0 {
			errs.appendError(`%d is not in %d`, append(path, `multipleOf`), iSub, iSup)
		}
	}
	return
}

func getSchema(items1 interface{}, items2 interface{}) (*jsonschema.Schema, *jsonschema.Schema, bool) {
	s1, ok1 := items1.(*jsonschema.Schema)
	s2, ok2 := items2.(*jsonschema.Schema)
	if ok1 && ok2 {
		return s1, s2, true
	} else {
		return nil, nil, false
	}
}

func getSchemaProperties(items1 interface{}, items2 interface{}) ([]*jsonschema.Schema, []*jsonschema.Schema, bool) {
	s1, ok1 := items1.([]*jsonschema.Schema)
	s2, ok2 := items2.([]*jsonschema.Schema)
	if ok1 && ok2 {
		return s1, s2, true
	} else {
		return nil, nil, false
	}
}

func compareArray(subset *jsonschema.Schema, superset *jsonschema.Schema, path []string) (errs validationErrors) {
	// if minItems is defined on subset but unspecified on superset or lesser
	if subset.MinItems != -1 {
		if superset.MinItems == -1 {
			errs.appendError(`array minItems is %d but superset has no minItems`,
				append(path, `minItems`), subset.MinItems)
		} else if superset.MinItems < subset.MinItems {
			errs.appendError(`array minItems is %d but superset has no minItems %d`,
				append(path, `minItems`), subset.MinItems, superset.MinItems)
		} // else: superset is greater than the subset
	}
	// if maxItems is defined on subset but unspecified on superset or greater
	if subset.MaxItems != -1 {
		if superset.MaxItems == -1 {
			errs.appendError(`array maxItems is %d but superset has no maxItems`,
				append(path, `maxItems`), subset.MinItems)
		} else if superset.MaxItems > subset.MaxItems {
			errs.appendError(`array maxItems is %d but superset has no maxItems %d`,
				append(path, `maxItems`), subset.MinItems, superset.MinItems)
		} // else: superset is greater than the subset
	}

	if subset.Items != nil {
		if superset.Items == nil {
			errs.appendError(`items only defined on subset`, append(path, `items`))
		} else {
			if subsetItemSchema, supersetItemSchema, ok := getSchema(subset.Items, superset.Items); ok {
				errs.append(compare3(subsetItemSchema, supersetItemSchema, append(path, `items`)))
			} else if subsetItemsSchema, supersetItemsSchema, ok := getSchemaProperties(subset.Items, superset.Items); ok {
				if len(subsetItemsSchema) != len(supersetItemsSchema) {
					errs.appendError(`sub-schema list have different length`, append(path, `items`))
				} else {
					for i := 0; i < len(subsetItemsSchema); i++ {
						itemPath := fmt.Sprintf(`item[%d]`, i)
						errs.append(compare3(subsetItemsSchema[i], supersetItemsSchema[i], append(path, itemPath)))
					}
				}
			} else {
				errs.appendError(`cannot match sub-schema list`, append(path, `items`))
			}
		}
	}

	if subset.AdditionalItems != nil || superset.AdditionalItems != nil {
		errs.notImplemented(path, `additionalItems`)
	}

	if subset.UniqueItems && !superset.UniqueItems {
		errs.appendError(`subset has uniqueItems=true but superset has not`, append(path, `uniqueItems`))
	}
	return
}

func resolveRef(schema *jsonschema.Schema) *jsonschema.Schema {
	if schema == nil {
		return nil
	}
	if schema.Ref != nil {
		return resolveRef(schema.Ref)
	}
	return schema
}

func deepCompare(schema1 *jsonschema.Schema, schema2 *jsonschema.Schema, path []string) (errs validationErrors) {
	schema1 = resolveRef(schema1)
	schema2 = resolveRef(schema2)
	if schema1 == nil && schema2 != nil || schema1 != nil && schema2 == nil {
		errs.appendError(`one schemas is defined while other is not defined`, path)
		return
	}

	// always
	if schema1.Always == nil && schema2.Always != nil || schema1.Always != nil && schema2.Always == nil {
		errs.appendError(`one schema is always true/false while the other is not defined`, path)
		return
	}
	if schema1.Always != nil && schema2.Always != nil &&
		*schema1.Always != *schema2.Always {
		errs.appendError(`one schema is always true/false while the other is the opposite`, path)
		return
	}

	errs = append(errs, compare3(schema1, schema2, path)...)
	return
}
